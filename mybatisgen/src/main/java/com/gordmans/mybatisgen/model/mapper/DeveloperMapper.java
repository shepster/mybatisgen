package com.gordmans.mybatisgen.model.mapper;

import com.gordmans.mybatisgen.model.Developer;
import com.gordmans.mybatisgen.model.DeveloperExample;
import java.math.BigDecimal;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DeveloperMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.DEVELOPER
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int countByExample(DeveloperExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.DEVELOPER
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int deleteByExample(DeveloperExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.DEVELOPER
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int deleteByPrimaryKey(BigDecimal id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.DEVELOPER
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int insert(Developer record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.DEVELOPER
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int insertSelective(Developer record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.DEVELOPER
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    List<Developer> selectByExample(DeveloperExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.DEVELOPER
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    Developer selectByPrimaryKey(BigDecimal id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.DEVELOPER
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int updateByExampleSelective(@Param("record") Developer record, @Param("example") DeveloperExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.DEVELOPER
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int updateByExample(@Param("record") Developer record, @Param("example") DeveloperExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.DEVELOPER
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int updateByPrimaryKeySelective(Developer record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.DEVELOPER
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int updateByPrimaryKey(Developer record);
}