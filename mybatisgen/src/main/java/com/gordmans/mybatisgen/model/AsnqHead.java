package com.gordmans.mybatisgen.model;

import java.math.BigDecimal;
import java.util.Date;

public class AsnqHead {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_HEAD.ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private BigDecimal id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_HEAD.ASN_NO
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String asnNo;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_HEAD.TIME_TO_PUBLISH
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date timeToPublish;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_HEAD.TO_LOC
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Long toLoc;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_HEAD.FROM_LOC
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Long fromLoc;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_HEAD.DISTRO_NO
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Long distroNo;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_HEAD.DISTRO_DOC_TYPE
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String distroDocType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_HEAD.LAST_UPDATE_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String lastUpdateId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_HEAD.LAST_UPDATE_DATETIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date lastUpdateDatetime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_HEAD.CREATE_DATETIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date createDatetime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_HEAD.STATUS
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String status;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_HEAD.ID
     *
     * @return the value of JBAPPOWN.ASNQ_HEAD.ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_HEAD.ID
     *
     * @param id the value for JBAPPOWN.ASNQ_HEAD.ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setId(BigDecimal id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_HEAD.ASN_NO
     *
     * @return the value of JBAPPOWN.ASNQ_HEAD.ASN_NO
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_HEAD.ASN_NO
     *
     * @param asnNo the value for JBAPPOWN.ASNQ_HEAD.ASN_NO
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo == null ? null : asnNo.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_HEAD.TIME_TO_PUBLISH
     *
     * @return the value of JBAPPOWN.ASNQ_HEAD.TIME_TO_PUBLISH
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getTimeToPublish() {
        return timeToPublish;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_HEAD.TIME_TO_PUBLISH
     *
     * @param timeToPublish the value for JBAPPOWN.ASNQ_HEAD.TIME_TO_PUBLISH
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setTimeToPublish(Date timeToPublish) {
        this.timeToPublish = timeToPublish;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_HEAD.TO_LOC
     *
     * @return the value of JBAPPOWN.ASNQ_HEAD.TO_LOC
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Long getToLoc() {
        return toLoc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_HEAD.TO_LOC
     *
     * @param toLoc the value for JBAPPOWN.ASNQ_HEAD.TO_LOC
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setToLoc(Long toLoc) {
        this.toLoc = toLoc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_HEAD.FROM_LOC
     *
     * @return the value of JBAPPOWN.ASNQ_HEAD.FROM_LOC
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Long getFromLoc() {
        return fromLoc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_HEAD.FROM_LOC
     *
     * @param fromLoc the value for JBAPPOWN.ASNQ_HEAD.FROM_LOC
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setFromLoc(Long fromLoc) {
        this.fromLoc = fromLoc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_HEAD.DISTRO_NO
     *
     * @return the value of JBAPPOWN.ASNQ_HEAD.DISTRO_NO
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Long getDistroNo() {
        return distroNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_HEAD.DISTRO_NO
     *
     * @param distroNo the value for JBAPPOWN.ASNQ_HEAD.DISTRO_NO
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setDistroNo(Long distroNo) {
        this.distroNo = distroNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_HEAD.DISTRO_DOC_TYPE
     *
     * @return the value of JBAPPOWN.ASNQ_HEAD.DISTRO_DOC_TYPE
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getDistroDocType() {
        return distroDocType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_HEAD.DISTRO_DOC_TYPE
     *
     * @param distroDocType the value for JBAPPOWN.ASNQ_HEAD.DISTRO_DOC_TYPE
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setDistroDocType(String distroDocType) {
        this.distroDocType = distroDocType == null ? null : distroDocType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_HEAD.LAST_UPDATE_ID
     *
     * @return the value of JBAPPOWN.ASNQ_HEAD.LAST_UPDATE_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getLastUpdateId() {
        return lastUpdateId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_HEAD.LAST_UPDATE_ID
     *
     * @param lastUpdateId the value for JBAPPOWN.ASNQ_HEAD.LAST_UPDATE_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setLastUpdateId(String lastUpdateId) {
        this.lastUpdateId = lastUpdateId == null ? null : lastUpdateId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_HEAD.LAST_UPDATE_DATETIME
     *
     * @return the value of JBAPPOWN.ASNQ_HEAD.LAST_UPDATE_DATETIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_HEAD.LAST_UPDATE_DATETIME
     *
     * @param lastUpdateDatetime the value for JBAPPOWN.ASNQ_HEAD.LAST_UPDATE_DATETIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setLastUpdateDatetime(Date lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_HEAD.CREATE_DATETIME
     *
     * @return the value of JBAPPOWN.ASNQ_HEAD.CREATE_DATETIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getCreateDatetime() {
        return createDatetime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_HEAD.CREATE_DATETIME
     *
     * @param createDatetime the value for JBAPPOWN.ASNQ_HEAD.CREATE_DATETIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_HEAD.STATUS
     *
     * @return the value of JBAPPOWN.ASNQ_HEAD.STATUS
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_HEAD.STATUS
     *
     * @param status the value for JBAPPOWN.ASNQ_HEAD.STATUS
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
}