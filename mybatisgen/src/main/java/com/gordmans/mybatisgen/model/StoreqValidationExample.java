package com.gordmans.mybatisgen.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StoreqValidationExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public StoreqValidationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(BigDecimal value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(BigDecimal value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(BigDecimal value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(BigDecimal value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(BigDecimal value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<BigDecimal> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<BigDecimal> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCartonLabelIsNull() {
            addCriterion("CARTON_LABEL is null");
            return (Criteria) this;
        }

        public Criteria andCartonLabelIsNotNull() {
            addCriterion("CARTON_LABEL is not null");
            return (Criteria) this;
        }

        public Criteria andCartonLabelEqualTo(String value) {
            addCriterion("CARTON_LABEL =", value, "cartonLabel");
            return (Criteria) this;
        }

        public Criteria andCartonLabelNotEqualTo(String value) {
            addCriterion("CARTON_LABEL <>", value, "cartonLabel");
            return (Criteria) this;
        }

        public Criteria andCartonLabelGreaterThan(String value) {
            addCriterion("CARTON_LABEL >", value, "cartonLabel");
            return (Criteria) this;
        }

        public Criteria andCartonLabelGreaterThanOrEqualTo(String value) {
            addCriterion("CARTON_LABEL >=", value, "cartonLabel");
            return (Criteria) this;
        }

        public Criteria andCartonLabelLessThan(String value) {
            addCriterion("CARTON_LABEL <", value, "cartonLabel");
            return (Criteria) this;
        }

        public Criteria andCartonLabelLessThanOrEqualTo(String value) {
            addCriterion("CARTON_LABEL <=", value, "cartonLabel");
            return (Criteria) this;
        }

        public Criteria andCartonLabelLike(String value) {
            addCriterion("CARTON_LABEL like", value, "cartonLabel");
            return (Criteria) this;
        }

        public Criteria andCartonLabelNotLike(String value) {
            addCriterion("CARTON_LABEL not like", value, "cartonLabel");
            return (Criteria) this;
        }

        public Criteria andCartonLabelIn(List<String> values) {
            addCriterion("CARTON_LABEL in", values, "cartonLabel");
            return (Criteria) this;
        }

        public Criteria andCartonLabelNotIn(List<String> values) {
            addCriterion("CARTON_LABEL not in", values, "cartonLabel");
            return (Criteria) this;
        }

        public Criteria andCartonLabelBetween(String value1, String value2) {
            addCriterion("CARTON_LABEL between", value1, value2, "cartonLabel");
            return (Criteria) this;
        }

        public Criteria andCartonLabelNotBetween(String value1, String value2) {
            addCriterion("CARTON_LABEL not between", value1, value2, "cartonLabel");
            return (Criteria) this;
        }

        public Criteria andDocTypeIsNull() {
            addCriterion("DOC_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andDocTypeIsNotNull() {
            addCriterion("DOC_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andDocTypeEqualTo(String value) {
            addCriterion("DOC_TYPE =", value, "docType");
            return (Criteria) this;
        }

        public Criteria andDocTypeNotEqualTo(String value) {
            addCriterion("DOC_TYPE <>", value, "docType");
            return (Criteria) this;
        }

        public Criteria andDocTypeGreaterThan(String value) {
            addCriterion("DOC_TYPE >", value, "docType");
            return (Criteria) this;
        }

        public Criteria andDocTypeGreaterThanOrEqualTo(String value) {
            addCriterion("DOC_TYPE >=", value, "docType");
            return (Criteria) this;
        }

        public Criteria andDocTypeLessThan(String value) {
            addCriterion("DOC_TYPE <", value, "docType");
            return (Criteria) this;
        }

        public Criteria andDocTypeLessThanOrEqualTo(String value) {
            addCriterion("DOC_TYPE <=", value, "docType");
            return (Criteria) this;
        }

        public Criteria andDocTypeLike(String value) {
            addCriterion("DOC_TYPE like", value, "docType");
            return (Criteria) this;
        }

        public Criteria andDocTypeNotLike(String value) {
            addCriterion("DOC_TYPE not like", value, "docType");
            return (Criteria) this;
        }

        public Criteria andDocTypeIn(List<String> values) {
            addCriterion("DOC_TYPE in", values, "docType");
            return (Criteria) this;
        }

        public Criteria andDocTypeNotIn(List<String> values) {
            addCriterion("DOC_TYPE not in", values, "docType");
            return (Criteria) this;
        }

        public Criteria andDocTypeBetween(String value1, String value2) {
            addCriterion("DOC_TYPE between", value1, value2, "docType");
            return (Criteria) this;
        }

        public Criteria andDocTypeNotBetween(String value1, String value2) {
            addCriterion("DOC_TYPE not between", value1, value2, "docType");
            return (Criteria) this;
        }

        public Criteria andDocNoIsNull() {
            addCriterion("DOC_NO is null");
            return (Criteria) this;
        }

        public Criteria andDocNoIsNotNull() {
            addCriterion("DOC_NO is not null");
            return (Criteria) this;
        }

        public Criteria andDocNoEqualTo(Long value) {
            addCriterion("DOC_NO =", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoNotEqualTo(Long value) {
            addCriterion("DOC_NO <>", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoGreaterThan(Long value) {
            addCriterion("DOC_NO >", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoGreaterThanOrEqualTo(Long value) {
            addCriterion("DOC_NO >=", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoLessThan(Long value) {
            addCriterion("DOC_NO <", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoLessThanOrEqualTo(Long value) {
            addCriterion("DOC_NO <=", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoIn(List<Long> values) {
            addCriterion("DOC_NO in", values, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoNotIn(List<Long> values) {
            addCriterion("DOC_NO not in", values, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoBetween(Long value1, Long value2) {
            addCriterion("DOC_NO between", value1, value2, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoNotBetween(Long value1, Long value2) {
            addCriterion("DOC_NO not between", value1, value2, "docNo");
            return (Criteria) this;
        }

        public Criteria andToLocIsNull() {
            addCriterion("TO_LOC is null");
            return (Criteria) this;
        }

        public Criteria andToLocIsNotNull() {
            addCriterion("TO_LOC is not null");
            return (Criteria) this;
        }

        public Criteria andToLocEqualTo(Long value) {
            addCriterion("TO_LOC =", value, "toLoc");
            return (Criteria) this;
        }

        public Criteria andToLocNotEqualTo(Long value) {
            addCriterion("TO_LOC <>", value, "toLoc");
            return (Criteria) this;
        }

        public Criteria andToLocGreaterThan(Long value) {
            addCriterion("TO_LOC >", value, "toLoc");
            return (Criteria) this;
        }

        public Criteria andToLocGreaterThanOrEqualTo(Long value) {
            addCriterion("TO_LOC >=", value, "toLoc");
            return (Criteria) this;
        }

        public Criteria andToLocLessThan(Long value) {
            addCriterion("TO_LOC <", value, "toLoc");
            return (Criteria) this;
        }

        public Criteria andToLocLessThanOrEqualTo(Long value) {
            addCriterion("TO_LOC <=", value, "toLoc");
            return (Criteria) this;
        }

        public Criteria andToLocIn(List<Long> values) {
            addCriterion("TO_LOC in", values, "toLoc");
            return (Criteria) this;
        }

        public Criteria andToLocNotIn(List<Long> values) {
            addCriterion("TO_LOC not in", values, "toLoc");
            return (Criteria) this;
        }

        public Criteria andToLocBetween(Long value1, Long value2) {
            addCriterion("TO_LOC between", value1, value2, "toLoc");
            return (Criteria) this;
        }

        public Criteria andToLocNotBetween(Long value1, Long value2) {
            addCriterion("TO_LOC not between", value1, value2, "toLoc");
            return (Criteria) this;
        }

        public Criteria andFromLocIsNull() {
            addCriterion("FROM_LOC is null");
            return (Criteria) this;
        }

        public Criteria andFromLocIsNotNull() {
            addCriterion("FROM_LOC is not null");
            return (Criteria) this;
        }

        public Criteria andFromLocEqualTo(Long value) {
            addCriterion("FROM_LOC =", value, "fromLoc");
            return (Criteria) this;
        }

        public Criteria andFromLocNotEqualTo(Long value) {
            addCriterion("FROM_LOC <>", value, "fromLoc");
            return (Criteria) this;
        }

        public Criteria andFromLocGreaterThan(Long value) {
            addCriterion("FROM_LOC >", value, "fromLoc");
            return (Criteria) this;
        }

        public Criteria andFromLocGreaterThanOrEqualTo(Long value) {
            addCriterion("FROM_LOC >=", value, "fromLoc");
            return (Criteria) this;
        }

        public Criteria andFromLocLessThan(Long value) {
            addCriterion("FROM_LOC <", value, "fromLoc");
            return (Criteria) this;
        }

        public Criteria andFromLocLessThanOrEqualTo(Long value) {
            addCriterion("FROM_LOC <=", value, "fromLoc");
            return (Criteria) this;
        }

        public Criteria andFromLocIn(List<Long> values) {
            addCriterion("FROM_LOC in", values, "fromLoc");
            return (Criteria) this;
        }

        public Criteria andFromLocNotIn(List<Long> values) {
            addCriterion("FROM_LOC not in", values, "fromLoc");
            return (Criteria) this;
        }

        public Criteria andFromLocBetween(Long value1, Long value2) {
            addCriterion("FROM_LOC between", value1, value2, "fromLoc");
            return (Criteria) this;
        }

        public Criteria andFromLocNotBetween(Long value1, Long value2) {
            addCriterion("FROM_LOC not between", value1, value2, "fromLoc");
            return (Criteria) this;
        }

        public Criteria andItemIsNull() {
            addCriterion("ITEM is null");
            return (Criteria) this;
        }

        public Criteria andItemIsNotNull() {
            addCriterion("ITEM is not null");
            return (Criteria) this;
        }

        public Criteria andItemEqualTo(String value) {
            addCriterion("ITEM =", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemNotEqualTo(String value) {
            addCriterion("ITEM <>", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemGreaterThan(String value) {
            addCriterion("ITEM >", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemGreaterThanOrEqualTo(String value) {
            addCriterion("ITEM >=", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemLessThan(String value) {
            addCriterion("ITEM <", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemLessThanOrEqualTo(String value) {
            addCriterion("ITEM <=", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemLike(String value) {
            addCriterion("ITEM like", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemNotLike(String value) {
            addCriterion("ITEM not like", value, "item");
            return (Criteria) this;
        }

        public Criteria andItemIn(List<String> values) {
            addCriterion("ITEM in", values, "item");
            return (Criteria) this;
        }

        public Criteria andItemNotIn(List<String> values) {
            addCriterion("ITEM not in", values, "item");
            return (Criteria) this;
        }

        public Criteria andItemBetween(String value1, String value2) {
            addCriterion("ITEM between", value1, value2, "item");
            return (Criteria) this;
        }

        public Criteria andItemNotBetween(String value1, String value2) {
            addCriterion("ITEM not between", value1, value2, "item");
            return (Criteria) this;
        }

        public Criteria andUnitQtyIsNull() {
            addCriterion("UNIT_QTY is null");
            return (Criteria) this;
        }

        public Criteria andUnitQtyIsNotNull() {
            addCriterion("UNIT_QTY is not null");
            return (Criteria) this;
        }

        public Criteria andUnitQtyEqualTo(Integer value) {
            addCriterion("UNIT_QTY =", value, "unitQty");
            return (Criteria) this;
        }

        public Criteria andUnitQtyNotEqualTo(Integer value) {
            addCriterion("UNIT_QTY <>", value, "unitQty");
            return (Criteria) this;
        }

        public Criteria andUnitQtyGreaterThan(Integer value) {
            addCriterion("UNIT_QTY >", value, "unitQty");
            return (Criteria) this;
        }

        public Criteria andUnitQtyGreaterThanOrEqualTo(Integer value) {
            addCriterion("UNIT_QTY >=", value, "unitQty");
            return (Criteria) this;
        }

        public Criteria andUnitQtyLessThan(Integer value) {
            addCriterion("UNIT_QTY <", value, "unitQty");
            return (Criteria) this;
        }

        public Criteria andUnitQtyLessThanOrEqualTo(Integer value) {
            addCriterion("UNIT_QTY <=", value, "unitQty");
            return (Criteria) this;
        }

        public Criteria andUnitQtyIn(List<Integer> values) {
            addCriterion("UNIT_QTY in", values, "unitQty");
            return (Criteria) this;
        }

        public Criteria andUnitQtyNotIn(List<Integer> values) {
            addCriterion("UNIT_QTY not in", values, "unitQty");
            return (Criteria) this;
        }

        public Criteria andUnitQtyBetween(Integer value1, Integer value2) {
            addCriterion("UNIT_QTY between", value1, value2, "unitQty");
            return (Criteria) this;
        }

        public Criteria andUnitQtyNotBetween(Integer value1, Integer value2) {
            addCriterion("UNIT_QTY not between", value1, value2, "unitQty");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdIsNull() {
            addCriterion("LAST_UPDATE_ID is null");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdIsNotNull() {
            addCriterion("LAST_UPDATE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdEqualTo(String value) {
            addCriterion("LAST_UPDATE_ID =", value, "lastUpdateId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdNotEqualTo(String value) {
            addCriterion("LAST_UPDATE_ID <>", value, "lastUpdateId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdGreaterThan(String value) {
            addCriterion("LAST_UPDATE_ID >", value, "lastUpdateId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdGreaterThanOrEqualTo(String value) {
            addCriterion("LAST_UPDATE_ID >=", value, "lastUpdateId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdLessThan(String value) {
            addCriterion("LAST_UPDATE_ID <", value, "lastUpdateId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdLessThanOrEqualTo(String value) {
            addCriterion("LAST_UPDATE_ID <=", value, "lastUpdateId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdLike(String value) {
            addCriterion("LAST_UPDATE_ID like", value, "lastUpdateId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdNotLike(String value) {
            addCriterion("LAST_UPDATE_ID not like", value, "lastUpdateId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdIn(List<String> values) {
            addCriterion("LAST_UPDATE_ID in", values, "lastUpdateId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdNotIn(List<String> values) {
            addCriterion("LAST_UPDATE_ID not in", values, "lastUpdateId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdBetween(String value1, String value2) {
            addCriterion("LAST_UPDATE_ID between", value1, value2, "lastUpdateId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateIdNotBetween(String value1, String value2) {
            addCriterion("LAST_UPDATE_ID not between", value1, value2, "lastUpdateId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateDatetimeIsNull() {
            addCriterion("LAST_UPDATE_DATETIME is null");
            return (Criteria) this;
        }

        public Criteria andLastUpdateDatetimeIsNotNull() {
            addCriterion("LAST_UPDATE_DATETIME is not null");
            return (Criteria) this;
        }

        public Criteria andLastUpdateDatetimeEqualTo(Date value) {
            addCriterion("LAST_UPDATE_DATETIME =", value, "lastUpdateDatetime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateDatetimeNotEqualTo(Date value) {
            addCriterion("LAST_UPDATE_DATETIME <>", value, "lastUpdateDatetime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateDatetimeGreaterThan(Date value) {
            addCriterion("LAST_UPDATE_DATETIME >", value, "lastUpdateDatetime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("LAST_UPDATE_DATETIME >=", value, "lastUpdateDatetime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateDatetimeLessThan(Date value) {
            addCriterion("LAST_UPDATE_DATETIME <", value, "lastUpdateDatetime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("LAST_UPDATE_DATETIME <=", value, "lastUpdateDatetime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateDatetimeIn(List<Date> values) {
            addCriterion("LAST_UPDATE_DATETIME in", values, "lastUpdateDatetime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateDatetimeNotIn(List<Date> values) {
            addCriterion("LAST_UPDATE_DATETIME not in", values, "lastUpdateDatetime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateDatetimeBetween(Date value1, Date value2) {
            addCriterion("LAST_UPDATE_DATETIME between", value1, value2, "lastUpdateDatetime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("LAST_UPDATE_DATETIME not between", value1, value2, "lastUpdateDatetime");
            return (Criteria) this;
        }

        public Criteria andCreateDatetimeIsNull() {
            addCriterion("CREATE_DATETIME is null");
            return (Criteria) this;
        }

        public Criteria andCreateDatetimeIsNotNull() {
            addCriterion("CREATE_DATETIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDatetimeEqualTo(Date value) {
            addCriterion("CREATE_DATETIME =", value, "createDatetime");
            return (Criteria) this;
        }

        public Criteria andCreateDatetimeNotEqualTo(Date value) {
            addCriterion("CREATE_DATETIME <>", value, "createDatetime");
            return (Criteria) this;
        }

        public Criteria andCreateDatetimeGreaterThan(Date value) {
            addCriterion("CREATE_DATETIME >", value, "createDatetime");
            return (Criteria) this;
        }

        public Criteria andCreateDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATE_DATETIME >=", value, "createDatetime");
            return (Criteria) this;
        }

        public Criteria andCreateDatetimeLessThan(Date value) {
            addCriterion("CREATE_DATETIME <", value, "createDatetime");
            return (Criteria) this;
        }

        public Criteria andCreateDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("CREATE_DATETIME <=", value, "createDatetime");
            return (Criteria) this;
        }

        public Criteria andCreateDatetimeIn(List<Date> values) {
            addCriterion("CREATE_DATETIME in", values, "createDatetime");
            return (Criteria) this;
        }

        public Criteria andCreateDatetimeNotIn(List<Date> values) {
            addCriterion("CREATE_DATETIME not in", values, "createDatetime");
            return (Criteria) this;
        }

        public Criteria andCreateDatetimeBetween(Date value1, Date value2) {
            addCriterion("CREATE_DATETIME between", value1, value2, "createDatetime");
            return (Criteria) this;
        }

        public Criteria andCreateDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("CREATE_DATETIME not between", value1, value2, "createDatetime");
            return (Criteria) this;
        }

        public Criteria andCreatedIdIsNull() {
            addCriterion("CREATED_ID is null");
            return (Criteria) this;
        }

        public Criteria andCreatedIdIsNotNull() {
            addCriterion("CREATED_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedIdEqualTo(String value) {
            addCriterion("CREATED_ID =", value, "createdId");
            return (Criteria) this;
        }

        public Criteria andCreatedIdNotEqualTo(String value) {
            addCriterion("CREATED_ID <>", value, "createdId");
            return (Criteria) this;
        }

        public Criteria andCreatedIdGreaterThan(String value) {
            addCriterion("CREATED_ID >", value, "createdId");
            return (Criteria) this;
        }

        public Criteria andCreatedIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATED_ID >=", value, "createdId");
            return (Criteria) this;
        }

        public Criteria andCreatedIdLessThan(String value) {
            addCriterion("CREATED_ID <", value, "createdId");
            return (Criteria) this;
        }

        public Criteria andCreatedIdLessThanOrEqualTo(String value) {
            addCriterion("CREATED_ID <=", value, "createdId");
            return (Criteria) this;
        }

        public Criteria andCreatedIdLike(String value) {
            addCriterion("CREATED_ID like", value, "createdId");
            return (Criteria) this;
        }

        public Criteria andCreatedIdNotLike(String value) {
            addCriterion("CREATED_ID not like", value, "createdId");
            return (Criteria) this;
        }

        public Criteria andCreatedIdIn(List<String> values) {
            addCriterion("CREATED_ID in", values, "createdId");
            return (Criteria) this;
        }

        public Criteria andCreatedIdNotIn(List<String> values) {
            addCriterion("CREATED_ID not in", values, "createdId");
            return (Criteria) this;
        }

        public Criteria andCreatedIdBetween(String value1, String value2) {
            addCriterion("CREATED_ID between", value1, value2, "createdId");
            return (Criteria) this;
        }

        public Criteria andCreatedIdNotBetween(String value1, String value2) {
            addCriterion("CREATED_ID not between", value1, value2, "createdId");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated do_not_delete_during_merge Fri Jul 24 12:13:40 CDT 2015
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table JBAPPOWN.STOREQ_VALIDATION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value) {
            super();
            this.condition = condition;
            this.value = value;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.betweenValue = true;
        }
    }
}