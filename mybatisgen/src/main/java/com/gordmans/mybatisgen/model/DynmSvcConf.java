package com.gordmans.mybatisgen.model;

import java.math.BigDecimal;
import java.util.Date;

public class DynmSvcConf {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.DYNM_SVC_CONF.DYNM_SVC_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private BigDecimal dynmSvcId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.DYNM_SVC_CONF.CONF_SVC_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private BigDecimal confSvcId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private BigDecimal confItemId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_VALU_ST
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String confItemValuSt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_ACT_IND
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String confItemActInd;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_GRP_NAME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String confItemGrpName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.DYNM_SVC_CONF.CRTN_DTIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date crtnDtime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.DYNM_SVC_CONF.CRTN_USER_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String crtnUserId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.DYNM_SVC_CONF.LAST_UPTD_DTIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date lastUptdDtime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.DYNM_SVC_CONF.LAST_UPTD_USER_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String lastUptdUserId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.DYNM_SVC_CONF.VERSION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private BigDecimal version;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.DYNM_SVC_CONF.DYNM_SVC_ID
     *
     * @return the value of JBAPPOWN.DYNM_SVC_CONF.DYNM_SVC_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public BigDecimal getDynmSvcId() {
        return dynmSvcId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.DYNM_SVC_CONF.DYNM_SVC_ID
     *
     * @param dynmSvcId the value for JBAPPOWN.DYNM_SVC_CONF.DYNM_SVC_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setDynmSvcId(BigDecimal dynmSvcId) {
        this.dynmSvcId = dynmSvcId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.DYNM_SVC_CONF.CONF_SVC_ID
     *
     * @return the value of JBAPPOWN.DYNM_SVC_CONF.CONF_SVC_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public BigDecimal getConfSvcId() {
        return confSvcId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.DYNM_SVC_CONF.CONF_SVC_ID
     *
     * @param confSvcId the value for JBAPPOWN.DYNM_SVC_CONF.CONF_SVC_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setConfSvcId(BigDecimal confSvcId) {
        this.confSvcId = confSvcId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_ID
     *
     * @return the value of JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public BigDecimal getConfItemId() {
        return confItemId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_ID
     *
     * @param confItemId the value for JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setConfItemId(BigDecimal confItemId) {
        this.confItemId = confItemId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_VALU_ST
     *
     * @return the value of JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_VALU_ST
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getConfItemValuSt() {
        return confItemValuSt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_VALU_ST
     *
     * @param confItemValuSt the value for JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_VALU_ST
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setConfItemValuSt(String confItemValuSt) {
        this.confItemValuSt = confItemValuSt == null ? null : confItemValuSt.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_ACT_IND
     *
     * @return the value of JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_ACT_IND
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getConfItemActInd() {
        return confItemActInd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_ACT_IND
     *
     * @param confItemActInd the value for JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_ACT_IND
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setConfItemActInd(String confItemActInd) {
        this.confItemActInd = confItemActInd == null ? null : confItemActInd.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_GRP_NAME
     *
     * @return the value of JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_GRP_NAME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getConfItemGrpName() {
        return confItemGrpName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_GRP_NAME
     *
     * @param confItemGrpName the value for JBAPPOWN.DYNM_SVC_CONF.CONF_ITEM_GRP_NAME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setConfItemGrpName(String confItemGrpName) {
        this.confItemGrpName = confItemGrpName == null ? null : confItemGrpName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.DYNM_SVC_CONF.CRTN_DTIME
     *
     * @return the value of JBAPPOWN.DYNM_SVC_CONF.CRTN_DTIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getCrtnDtime() {
        return crtnDtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.DYNM_SVC_CONF.CRTN_DTIME
     *
     * @param crtnDtime the value for JBAPPOWN.DYNM_SVC_CONF.CRTN_DTIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setCrtnDtime(Date crtnDtime) {
        this.crtnDtime = crtnDtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.DYNM_SVC_CONF.CRTN_USER_ID
     *
     * @return the value of JBAPPOWN.DYNM_SVC_CONF.CRTN_USER_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getCrtnUserId() {
        return crtnUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.DYNM_SVC_CONF.CRTN_USER_ID
     *
     * @param crtnUserId the value for JBAPPOWN.DYNM_SVC_CONF.CRTN_USER_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setCrtnUserId(String crtnUserId) {
        this.crtnUserId = crtnUserId == null ? null : crtnUserId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.DYNM_SVC_CONF.LAST_UPTD_DTIME
     *
     * @return the value of JBAPPOWN.DYNM_SVC_CONF.LAST_UPTD_DTIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getLastUptdDtime() {
        return lastUptdDtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.DYNM_SVC_CONF.LAST_UPTD_DTIME
     *
     * @param lastUptdDtime the value for JBAPPOWN.DYNM_SVC_CONF.LAST_UPTD_DTIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setLastUptdDtime(Date lastUptdDtime) {
        this.lastUptdDtime = lastUptdDtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.DYNM_SVC_CONF.LAST_UPTD_USER_ID
     *
     * @return the value of JBAPPOWN.DYNM_SVC_CONF.LAST_UPTD_USER_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getLastUptdUserId() {
        return lastUptdUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.DYNM_SVC_CONF.LAST_UPTD_USER_ID
     *
     * @param lastUptdUserId the value for JBAPPOWN.DYNM_SVC_CONF.LAST_UPTD_USER_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setLastUptdUserId(String lastUptdUserId) {
        this.lastUptdUserId = lastUptdUserId == null ? null : lastUptdUserId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.DYNM_SVC_CONF.VERSION
     *
     * @return the value of JBAPPOWN.DYNM_SVC_CONF.VERSION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.DYNM_SVC_CONF.VERSION
     *
     * @param version the value for JBAPPOWN.DYNM_SVC_CONF.VERSION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setVersion(BigDecimal version) {
        this.version = version;
    }
}