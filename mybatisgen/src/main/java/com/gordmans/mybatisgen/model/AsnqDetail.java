package com.gordmans.mybatisgen.model;

import java.math.BigDecimal;
import java.util.Date;

public class AsnqDetail {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_DETAIL.ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private BigDecimal id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_DETAIL.ASN_NO
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String asnNo;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_DETAIL.ITEM
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String item;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_DETAIL.UNIT_QTY
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Integer unitQty;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_DETAIL.LAST_UPDATE_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String lastUpdateId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_DETAIL.LAST_UPDATE_DATETIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date lastUpdateDatetime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.ASNQ_DETAIL.CREATE_DATETIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date createDatetime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_DETAIL.ID
     *
     * @return the value of JBAPPOWN.ASNQ_DETAIL.ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_DETAIL.ID
     *
     * @param id the value for JBAPPOWN.ASNQ_DETAIL.ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setId(BigDecimal id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_DETAIL.ASN_NO
     *
     * @return the value of JBAPPOWN.ASNQ_DETAIL.ASN_NO
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_DETAIL.ASN_NO
     *
     * @param asnNo the value for JBAPPOWN.ASNQ_DETAIL.ASN_NO
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo == null ? null : asnNo.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_DETAIL.ITEM
     *
     * @return the value of JBAPPOWN.ASNQ_DETAIL.ITEM
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getItem() {
        return item;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_DETAIL.ITEM
     *
     * @param item the value for JBAPPOWN.ASNQ_DETAIL.ITEM
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setItem(String item) {
        this.item = item == null ? null : item.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_DETAIL.UNIT_QTY
     *
     * @return the value of JBAPPOWN.ASNQ_DETAIL.UNIT_QTY
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Integer getUnitQty() {
        return unitQty;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_DETAIL.UNIT_QTY
     *
     * @param unitQty the value for JBAPPOWN.ASNQ_DETAIL.UNIT_QTY
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setUnitQty(Integer unitQty) {
        this.unitQty = unitQty;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_DETAIL.LAST_UPDATE_ID
     *
     * @return the value of JBAPPOWN.ASNQ_DETAIL.LAST_UPDATE_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getLastUpdateId() {
        return lastUpdateId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_DETAIL.LAST_UPDATE_ID
     *
     * @param lastUpdateId the value for JBAPPOWN.ASNQ_DETAIL.LAST_UPDATE_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setLastUpdateId(String lastUpdateId) {
        this.lastUpdateId = lastUpdateId == null ? null : lastUpdateId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_DETAIL.LAST_UPDATE_DATETIME
     *
     * @return the value of JBAPPOWN.ASNQ_DETAIL.LAST_UPDATE_DATETIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_DETAIL.LAST_UPDATE_DATETIME
     *
     * @param lastUpdateDatetime the value for JBAPPOWN.ASNQ_DETAIL.LAST_UPDATE_DATETIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setLastUpdateDatetime(Date lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.ASNQ_DETAIL.CREATE_DATETIME
     *
     * @return the value of JBAPPOWN.ASNQ_DETAIL.CREATE_DATETIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getCreateDatetime() {
        return createDatetime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.ASNQ_DETAIL.CREATE_DATETIME
     *
     * @param createDatetime the value for JBAPPOWN.ASNQ_DETAIL.CREATE_DATETIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }
}