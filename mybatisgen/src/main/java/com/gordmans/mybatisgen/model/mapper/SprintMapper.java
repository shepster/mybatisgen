package com.gordmans.mybatisgen.model.mapper;

import com.gordmans.mybatisgen.model.Sprint;
import com.gordmans.mybatisgen.model.SprintExample;
import java.math.BigDecimal;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SprintMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.SPRINT
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int countByExample(SprintExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.SPRINT
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int deleteByExample(SprintExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.SPRINT
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int deleteByPrimaryKey(BigDecimal id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.SPRINT
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int insert(Sprint record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.SPRINT
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int insertSelective(Sprint record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.SPRINT
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    List<Sprint> selectByExample(SprintExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.SPRINT
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    Sprint selectByPrimaryKey(BigDecimal id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.SPRINT
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int updateByExampleSelective(@Param("record") Sprint record, @Param("example") SprintExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.SPRINT
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int updateByExample(@Param("record") Sprint record, @Param("example") SprintExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.SPRINT
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int updateByPrimaryKeySelective(Sprint record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table JBAPPOWN.SPRINT
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    int updateByPrimaryKey(Sprint record);
}