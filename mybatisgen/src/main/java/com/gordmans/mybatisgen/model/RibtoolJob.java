package com.gordmans.mybatisgen.model;

import java.math.BigDecimal;
import java.util.Date;

public class RibtoolJob {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.RIBTOOL_JOB.ROW_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private BigDecimal rowId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.RIBTOOL_JOB.NAME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.RIBTOOL_JOB.HOST
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String host;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.RIBTOOL_JOB.START_DATE
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date startDate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.RIBTOOL_JOB.END_DATE
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date endDate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.RIBTOOL_JOB.TEMPLATE_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private BigDecimal templateId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.RIBTOOL_JOB.STATUS
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String status;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.RIBTOOL_JOB.CREATED_TIMESTAMP
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date createdTimestamp;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.RIBTOOL_JOB.STARTED_TIMESTAMP
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date startedTimestamp;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.RIBTOOL_JOB.ENDED_TIMESTAMP
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date endedTimestamp;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.RIBTOOL_JOB.ROW_ID
     *
     * @return the value of JBAPPOWN.RIBTOOL_JOB.ROW_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public BigDecimal getRowId() {
        return rowId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.RIBTOOL_JOB.ROW_ID
     *
     * @param rowId the value for JBAPPOWN.RIBTOOL_JOB.ROW_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setRowId(BigDecimal rowId) {
        this.rowId = rowId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.RIBTOOL_JOB.NAME
     *
     * @return the value of JBAPPOWN.RIBTOOL_JOB.NAME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.RIBTOOL_JOB.NAME
     *
     * @param name the value for JBAPPOWN.RIBTOOL_JOB.NAME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.RIBTOOL_JOB.HOST
     *
     * @return the value of JBAPPOWN.RIBTOOL_JOB.HOST
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getHost() {
        return host;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.RIBTOOL_JOB.HOST
     *
     * @param host the value for JBAPPOWN.RIBTOOL_JOB.HOST
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setHost(String host) {
        this.host = host == null ? null : host.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.RIBTOOL_JOB.START_DATE
     *
     * @return the value of JBAPPOWN.RIBTOOL_JOB.START_DATE
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.RIBTOOL_JOB.START_DATE
     *
     * @param startDate the value for JBAPPOWN.RIBTOOL_JOB.START_DATE
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.RIBTOOL_JOB.END_DATE
     *
     * @return the value of JBAPPOWN.RIBTOOL_JOB.END_DATE
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.RIBTOOL_JOB.END_DATE
     *
     * @param endDate the value for JBAPPOWN.RIBTOOL_JOB.END_DATE
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.RIBTOOL_JOB.TEMPLATE_ID
     *
     * @return the value of JBAPPOWN.RIBTOOL_JOB.TEMPLATE_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public BigDecimal getTemplateId() {
        return templateId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.RIBTOOL_JOB.TEMPLATE_ID
     *
     * @param templateId the value for JBAPPOWN.RIBTOOL_JOB.TEMPLATE_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setTemplateId(BigDecimal templateId) {
        this.templateId = templateId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.RIBTOOL_JOB.STATUS
     *
     * @return the value of JBAPPOWN.RIBTOOL_JOB.STATUS
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.RIBTOOL_JOB.STATUS
     *
     * @param status the value for JBAPPOWN.RIBTOOL_JOB.STATUS
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.RIBTOOL_JOB.CREATED_TIMESTAMP
     *
     * @return the value of JBAPPOWN.RIBTOOL_JOB.CREATED_TIMESTAMP
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getCreatedTimestamp() {
        return createdTimestamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.RIBTOOL_JOB.CREATED_TIMESTAMP
     *
     * @param createdTimestamp the value for JBAPPOWN.RIBTOOL_JOB.CREATED_TIMESTAMP
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setCreatedTimestamp(Date createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.RIBTOOL_JOB.STARTED_TIMESTAMP
     *
     * @return the value of JBAPPOWN.RIBTOOL_JOB.STARTED_TIMESTAMP
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getStartedTimestamp() {
        return startedTimestamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.RIBTOOL_JOB.STARTED_TIMESTAMP
     *
     * @param startedTimestamp the value for JBAPPOWN.RIBTOOL_JOB.STARTED_TIMESTAMP
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setStartedTimestamp(Date startedTimestamp) {
        this.startedTimestamp = startedTimestamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.RIBTOOL_JOB.ENDED_TIMESTAMP
     *
     * @return the value of JBAPPOWN.RIBTOOL_JOB.ENDED_TIMESTAMP
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getEndedTimestamp() {
        return endedTimestamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.RIBTOOL_JOB.ENDED_TIMESTAMP
     *
     * @param endedTimestamp the value for JBAPPOWN.RIBTOOL_JOB.ENDED_TIMESTAMP
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setEndedTimestamp(Date endedTimestamp) {
        this.endedTimestamp = endedTimestamp;
    }
}