package com.gordmans.mybatisgen.model;

import java.math.BigDecimal;
import java.util.Date;

public class ConfSvcData {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.CONF_SVC_DATA.CONF_SVC_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private BigDecimal confSvcId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.CONF_SVC_DATA.CONF_SVC_NAME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String confSvcName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.CONF_SVC_DATA.CONF_SVC_DESC
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String confSvcDesc;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.CONF_SVC_DATA.CONF_SVC_ACT_IND
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String confSvcActInd;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.CONF_SVC_DATA.CRTN_DTIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date crtnDtime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.CONF_SVC_DATA.CRTN_USER_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String crtnUserId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.CONF_SVC_DATA.LAST_UPTD_DTIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private Date lastUptdDtime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.CONF_SVC_DATA.LAST_UPTD_USER_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private String lastUptdUserId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column JBAPPOWN.CONF_SVC_DATA.VERSION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    private BigDecimal version;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.CONF_SVC_DATA.CONF_SVC_ID
     *
     * @return the value of JBAPPOWN.CONF_SVC_DATA.CONF_SVC_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public BigDecimal getConfSvcId() {
        return confSvcId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.CONF_SVC_DATA.CONF_SVC_ID
     *
     * @param confSvcId the value for JBAPPOWN.CONF_SVC_DATA.CONF_SVC_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setConfSvcId(BigDecimal confSvcId) {
        this.confSvcId = confSvcId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.CONF_SVC_DATA.CONF_SVC_NAME
     *
     * @return the value of JBAPPOWN.CONF_SVC_DATA.CONF_SVC_NAME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getConfSvcName() {
        return confSvcName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.CONF_SVC_DATA.CONF_SVC_NAME
     *
     * @param confSvcName the value for JBAPPOWN.CONF_SVC_DATA.CONF_SVC_NAME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setConfSvcName(String confSvcName) {
        this.confSvcName = confSvcName == null ? null : confSvcName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.CONF_SVC_DATA.CONF_SVC_DESC
     *
     * @return the value of JBAPPOWN.CONF_SVC_DATA.CONF_SVC_DESC
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getConfSvcDesc() {
        return confSvcDesc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.CONF_SVC_DATA.CONF_SVC_DESC
     *
     * @param confSvcDesc the value for JBAPPOWN.CONF_SVC_DATA.CONF_SVC_DESC
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setConfSvcDesc(String confSvcDesc) {
        this.confSvcDesc = confSvcDesc == null ? null : confSvcDesc.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.CONF_SVC_DATA.CONF_SVC_ACT_IND
     *
     * @return the value of JBAPPOWN.CONF_SVC_DATA.CONF_SVC_ACT_IND
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getConfSvcActInd() {
        return confSvcActInd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.CONF_SVC_DATA.CONF_SVC_ACT_IND
     *
     * @param confSvcActInd the value for JBAPPOWN.CONF_SVC_DATA.CONF_SVC_ACT_IND
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setConfSvcActInd(String confSvcActInd) {
        this.confSvcActInd = confSvcActInd == null ? null : confSvcActInd.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.CONF_SVC_DATA.CRTN_DTIME
     *
     * @return the value of JBAPPOWN.CONF_SVC_DATA.CRTN_DTIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getCrtnDtime() {
        return crtnDtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.CONF_SVC_DATA.CRTN_DTIME
     *
     * @param crtnDtime the value for JBAPPOWN.CONF_SVC_DATA.CRTN_DTIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setCrtnDtime(Date crtnDtime) {
        this.crtnDtime = crtnDtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.CONF_SVC_DATA.CRTN_USER_ID
     *
     * @return the value of JBAPPOWN.CONF_SVC_DATA.CRTN_USER_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getCrtnUserId() {
        return crtnUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.CONF_SVC_DATA.CRTN_USER_ID
     *
     * @param crtnUserId the value for JBAPPOWN.CONF_SVC_DATA.CRTN_USER_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setCrtnUserId(String crtnUserId) {
        this.crtnUserId = crtnUserId == null ? null : crtnUserId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.CONF_SVC_DATA.LAST_UPTD_DTIME
     *
     * @return the value of JBAPPOWN.CONF_SVC_DATA.LAST_UPTD_DTIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public Date getLastUptdDtime() {
        return lastUptdDtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.CONF_SVC_DATA.LAST_UPTD_DTIME
     *
     * @param lastUptdDtime the value for JBAPPOWN.CONF_SVC_DATA.LAST_UPTD_DTIME
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setLastUptdDtime(Date lastUptdDtime) {
        this.lastUptdDtime = lastUptdDtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.CONF_SVC_DATA.LAST_UPTD_USER_ID
     *
     * @return the value of JBAPPOWN.CONF_SVC_DATA.LAST_UPTD_USER_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public String getLastUptdUserId() {
        return lastUptdUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.CONF_SVC_DATA.LAST_UPTD_USER_ID
     *
     * @param lastUptdUserId the value for JBAPPOWN.CONF_SVC_DATA.LAST_UPTD_USER_ID
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setLastUptdUserId(String lastUptdUserId) {
        this.lastUptdUserId = lastUptdUserId == null ? null : lastUptdUserId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column JBAPPOWN.CONF_SVC_DATA.VERSION
     *
     * @return the value of JBAPPOWN.CONF_SVC_DATA.VERSION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column JBAPPOWN.CONF_SVC_DATA.VERSION
     *
     * @param version the value for JBAPPOWN.CONF_SVC_DATA.VERSION
     *
     * @mbggenerated Fri Jul 24 12:13:40 CDT 2015
     */
    public void setVersion(BigDecimal version) {
        this.version = version;
    }
}